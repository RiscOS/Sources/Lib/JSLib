/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    JSLibAPI.h                                        */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Provide a clean, simple starting point for        */
/*          #includes required to use JSLib.                  */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 27-Apr-1998 (ADH): Created.                       */
/*          11-Apr-2000 (ADH): 64-wide comments adopted.      */
/*          25-May-2000 (ADH): Multiple inclusion protection. */
/**************************************************************/

#ifndef JSLib_JSLibAPI__
  #define JSLib_JSLibAPI__

  /* This is the main JavaScript interpreter API. It pulls in quite a   */
  /* few other header files itself, too - I recommend you follow the    */
  /* include path to see what you're getting (careful - not all of the  */
  /* headers may include everything at the top; other #includes may be  */
  /* placed further down in the body of the source).                    */

  #include "jsapi.h"

  /* Needed to configure the floating point status register so that it  */
  /* doesn't fault various floating point oddities that would otherwise */
  /* be complained about. The JavaScript engine expects this.           */
  /*                                                                    */
  /* Do something like:                                                 */
  /*                                                                    */
  /* set_fpsr(0, fpsr_trap_INX |                                        */
  /*             fpsr_trap_UFL |                                        */
  /*             fpsr_trap_OFL |                                        */
  /*             fpsr_trap_DVZ |                                        */
  /*             fpsr_trap_IVO);                                        */

  #include "SetFPSR.h"

#endif /* JSLib_JSLibAPI__ */
