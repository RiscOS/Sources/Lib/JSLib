# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for JSLib
#

COMPONENT = JSLib
OBJS      = SetFPSR\
            jsaddr jsapi jsarray jsatom jsbool jscntxt jsdate jsdbgapi jsemit jsfun\
            jsgc jsinterp jsmath jsnum jsobj jsopcode jsparse jsregexp jsscan jsscope\
            jsscript jsstr prmjtime # jslock - thread locking not needed right now
HDRS      = other_jslib_hdrs
CINCLUDES = ${TCPIPINC} ${NSPRINC} -IRISCOSconf -ICore
CDEFINES  = -DRISCOS -DNSPR20 -DJSFILE -DUSE_NSPRLIB_ALLOC_REGISTRATION
VPATH     = Core RISCOSconf
CFLAGS    = ${C_NOWARN_NON_ANSI_INCLUDES}

include CLibrary

exphdr.other_jslib_hdrs:
	${CP} Core${SEP}h                     ${EXPDIR}${SEP}h               ${CPFLAGS}
	${CP} RISCOSconf${SEP}h${SEP}SetFPSR  ${EXPDIR}${SEP}h${SEP}SetFPSR  ${CPFLAGS}
	${CP} RISCOSconf${SEP}h${SEP}JSLibAPI ${EXPDIR}${SEP}h${SEP}JSLibAPI ${CPFLAGS}

# Dynamic dependencies:
