# Copyright 2021 RISC OS Open Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for the Javascript interpreter application
#

COMPONENT = JSInterpreter
TARGET    = !RunImage
INSTTYPE ?= app
APP_OBJS  = js
DBG_OBJS  = js svcprint
CINCLUDES = ${TCPIPINC} ${NSPRINC} -IRISCOSconf -ICore
CDEFINES  = -DRISCOS -DNSPR20 -DJSFILE -DUSE_NSPRLIB_ALLOC_REGISTRATION
VPATH     = Core RISCOSconf
CFLAGS    = ${C_NOWARN_NON_ANSI_INCLUDES}
LIBS      = ${JSLIB} ${NSPRLIB}

include CApp

# Dynamic dependencies:
